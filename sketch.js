const WIDTH = 1000;
const HEIGHT = 700;
const STAR_NUM = 25;
const PARTNER_NUM = 3;
const MAX_PARTNER_DISTANCE = 300;
const GLOBAL_SPEED = 0.3;

let stars = [];

function setup() {
    createCanvas(WIDTH, HEIGHT);

    for (i = 0; i < STAR_NUM; i++) {
        stars.push({
            point: getRandomPoint(),
            partners: [],
            velocityx: getVelocity(),
            velocityy: getVelocity(),
        });
    }

    for (i = 0; i < STAR_NUM; i++) {
        for (j = 0; j < PARTNER_NUM; j++) {
            stars[i].partners.push(getPartner(stars[i]));
        }
    }

}

function getVelocity() {
    let v = Math.random();
    if (Math.random() > 0.5) {
        v *= -1;
    }

    return v;
}

function getPartner(c) {
    while (true) {
        let newPartner = Math.floor(Math.random() * Math.floor(STAR_NUM));

        if (newPartner != c && getDistance(c.point, stars[newPartner].point) < MAX_PARTNER_DISTANCE && !checkOffScreen(stars[newPartner].point)) {
            return stars[newPartner];
        }
    }
}

function getRandomPoint() {
    return {
        x: Math.floor(Math.random() * Math.floor(WIDTH)),
        y: Math.floor(Math.random() * Math.floor(HEIGHT))
    };
}

function getDistance(a, b) {
    return Math.sqrt(Math.pow((a.x - b.x), 2) + Math.pow(a.y - b.y, 2));
}

function checkOffScreen(p) {
    if (p.x < 0 || p.x > WIDTH || p.y < 0 || p.y > HEIGHT) {
        console.log("returing true");
        return true;
    } else {
        return false;
    }
}

function draw() {
    background(0);
    fill(255);
    stroke(255);
    strokeWeight(1);
    textSize(20);

    for (i = 0; i < STAR_NUM; i++) {
        let this_star = stars[i];
        circle(stars[i].point.x, this_star.point.y, 20);
        text(`(${Math.floor(stars[i].point.x)}x, ${Math.floor(stars[i].point.y)}y)`, stars[i].point.x, stars[i].point.y + 20);

        this_star.point.x += this_star.velocityx * GLOBAL_SPEED;
        this_star.point.y += this_star.velocityy * GLOBAL_SPEED;

        for (j = 0; j < this_star.partners.length; j++) {
            line(this_star.point.x, this_star.point.y, this_star.partners[j].point.x, this_star.partners[j].point.y);
        }

        let newPartners = [];

        for (j = 0; j < this_star.partners.length; j++) {
            // check if partner is off screen or too far away
            if (!checkOffScreen(this_star.point) && (getDistance(this_star.point, this_star.partners[j].point) > MAX_PARTNER_DISTANCE || checkOffScreen(this_star.partners[j].point))) {
                newPartners.push(getPartner(this_star));
            } else {
                newPartners.push(this_star.partners[j]);
            }
        }

        this_star.partners = newPartners;
    }

}